<?php include_once 'header.php'; ?>   

    <!-- CONTEUDO -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>polls</h5>
                    </div>
                    <?php echo mostraMensagem(); ?>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <a href="<?php echo $path ?>pollsAdd" class="btn btn-primary">
                                    Adicionar
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-12 marginTop">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $query = "SELECT * FROM polls ORDER BY poll_description ASC";
                                                $query = mysql_query($query);
                                                while ($res = mysql_fetch_array($query)) {
                                                    $id = $res["poll_id"];                                                    
                                                    $titulo = $res["poll_description"];
                                                    echo
                                                    '
                                                        <tr>
                                                            <td>'.$titulo.'</td>
                                                            <td class="tdBtn text-center">
                                                                <a class="btn btn-primary" href="perguntas.php?idpoll='.$id.'">
                                                                    Perguntas
                                                                </a>
                                                            </td>
                                                            <td class="tdBtn text-center">
                                                                <a href="'.$path.'pollsEdit/'.$id.'" class="btn btn-info">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </a>
                                                                ';?>
                                                                <span class="btn btn-danger" 
                                                                onclick="delete_(<?php echo $id ?>, 'poll' )">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </span>
                                                                <?php
                                                                echo
                                                                '
                                                            </td>
                                                        </tr>
                                                    ';
                                                }

                                            ?>

                                        </tbody>
                                    </table>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIM CONTEUDO -->

<script type="text/javascript" src="script.js"></script>
<?php include_once 'footer.php'; ?>
