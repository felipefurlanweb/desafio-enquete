<?php 


function VoltaMensagem($msg, $tipo){

	setaMensagem($msg, $tipo);

	voltaPaginaAnterior();

}


function setaMensagem($msg, $tipo){

	if (!isset($_SESSION['mensagens'])) {

		$_SESSION['mensagens'] = array();

	}

	$_SESSION['mensagens'][$msg] = $tipo;

}



function Mensagens(){

	$msgs = array();

	if (isset($_SESSION['mensagens'])) {

		$msgs = $_SESSION['mensagens'];

	}

	limpaMensagens();

	return $msgs;

}



function limpaMensagens(){

	if (@isset($_SESSION['mensagens'])) {

		unset($_SESSION['mensagens']);

	}

}


function mostraMensagem($style=""){

	$mensagens = "";

	if ($style != "") {

		$style = 'style="'.$style.'"';

	}

	$mens = array();

	$msgs = Mensagens();

	if (!empty($msgs)) {

		foreach ($msgs as $_mensagem => $tipo) {

			$mens[] = $_mensagem;

		}

		$mensagens = '<div class="'.$tipo.'" '.$style.'>'.implode("<br>\n", $mens).'</div>';

	}

	return $mensagens;

}



function goToPage($pagina){

	goToUrl($pagina);

}



function goToUrl($url){

	header("Location: ".$url);

	exit(0);

}



?>