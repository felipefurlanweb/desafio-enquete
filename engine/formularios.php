<?php 
if (!isset($_SESSION["poll"])) {
    session_start();
}
    include_once 'funcoes.php';
    include_once '../banco.php';

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $pathURL = 'http://'.$_SERVER['HTTP_HOST'].'/enquete/';
    
    $id = $_POST['id'];
    switch ($id) {

        case 'pollAdd':
            pollAdd($pathURL);
            break;
        case 'pollEdit':
            pollEdit($pathURL);
            break;
        case 'pollRemove':
            pollRemove();
            break;

        case 'optionAdd':
            optionAdd($pathURL);
            break;
        case 'optionEdit':
            optionEdit($pathURL);
            break;
        case 'optionRemove':
            optionRemove();
            break;

        case 'vote':
            vote($pathURL);
            break;

            

    }

    // FUNÇÕES

    ///////////////////////////////////////////////////////////////////////////////////////////////

    function vote($pathURL){
        $idpoll = $_POST["idpoll"];
        $option_id = $_POST["option_id"];
        $query = "INSERT INTO votes (option_id, count) VALUES ($option_id, 1)";
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Voto realizado com sucesso", "alert alert-success");
            goToPage($pathURL."options/$idpoll");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage($pathURL."options/$idpoll");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    function optionAdd($pathURL){
        $idpoll = $_POST["idModel"];
        $titulo = $_POST["titulo"];
        $query = "INSERT INTO options (poll_id, option_description) VALUES ('%s', '%s')";
        $query = sprintf($query, addslashes($idpoll), addslashes($titulo));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro realizado com sucesso", "alert alert-success");
            goToPage($pathURL."options/$idpoll");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage($pathURL."options/$idpoll");
        }
    }
    
    function optionEdit($pathURL){
        $id = $_POST["idModel"];
        $idpoll = $_POST["idpoll"];
        $titulo = $_POST["titulo"];
        $query = "UPDATE options SET option_description = '%s' WHERE option_id = $id";
        $query = sprintf($query, addslashes($titulo));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro alterado com sucesso", "alert alert-success");
            goToPage($pathURL."options/$idpoll");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage($pathURL."options/$idpoll");
        }
    }

    function optionRemove(){
        $id = $_POST["idModel"];
        $query = "DELETE FROM options WHERE option_id = $id";
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro deletado com sucesso", "alert alert-success");
            echo '1';
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            echo '0';
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////

    function pollAdd($pathURL){
        $titulo = $_POST["titulo"];
        $query = "INSERT INTO polls (poll_description) VALUES ('%s')";
        $query = sprintf($query, addslashes($titulo));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro realizado com sucesso", "alert alert-success");
            goToPage($pathURL."polls");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage($pathURL."polls");
        }
    }
    
    function pollEdit($pathURL){
        $id = $_POST["idModel"];
        $titulo = $_POST["titulo"];
        $query = "UPDATE polls SET poll_description = '%s' WHERE poll_id = $id";
        $query = sprintf($query, addslashes($titulo));
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro alterado com sucesso", "alert alert-success");
            goToPage($pathURL."polls");
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            goToPage($pathURL."polls");
        }
    }

    function pollRemove(){
        $id = $_POST["idModel"];
        $param = $_POST["param"];
        $query = "DELETE FROM polls WHERE poll_id = $id";
        $query = mysql_query($query);
        if ($query) {
            setaMensagem("Cadastro deletado com sucesso", "alert alert-success");
            echo '1';
        }else{
            setaMensagem("Algo deu errado, tente novamente", "alert alert-danger");
            echo '0';
        }
    }

