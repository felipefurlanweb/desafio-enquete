<?php include_once 'header.php'; ?>
    
    <!-- CONTEUDO -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Adicionar Enquete</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form action="<?php echo $path; ?>engine/formularios.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="pollAdd">
                                <div class="col-xs-12 col-md-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 form-group">
                                            <label>Descrição</label>
                                            <input type="text" class="form-control" name="titulo" required>
                                        </div>                                     
                                    </div>                                     
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 marginTop text-right">
                                            <hr>
                                            <a href="<?php echo $path; ?>polls" class="btn btn-danger">Cancelar</a>
                                            <input type="submit" class="btn btn-primary" value="Salvar">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIM CONTEUDO -->

<?php include_once 'footer.php'; ?>
