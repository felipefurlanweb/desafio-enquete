<html>
<head>
	<title></title>
	<meta charset="utf-8">
</head>
<body>
	<p>Desafio Enquete - LUXFACTA</p>
	<hr>
	<p><b>Configurações</b></p>
	<p>No diretório do projeto, existe um arquivo chamado: "banco.php", nele está as configurações de banco de dados.</p>
	<p>No diretório do projeto, existe um arquivo chamado: "header.php", nele está declarada a variável "$path", que passa a url do projeto para que fique url amigável, exemplo desse projeto está assim: </p>
	<code>$path = 'http://'.$_SERVER['HTTP_HOST'].'/enquete/';</code>
	<p>onde "enquete" é a pasta raiz, alterar para a pasta raiz onde vai inserir o projeto</p>
	<p>No diretório do projeto, acesse a pasta "engine", dentro dela, existe um arquivo chamado: "formularios.php", nele está declarada a variável "$pathURL", que recebe o seguinte texto:</p>
	<code>$pathURL = 'http://'.$_SERVER['HTTP_HOST'].'/enquete/';</code>
	<p>onde "enquete" é a pasta raiz, alterar para a pasta raiz onde vai inserir o projeto</p>
	<p>No diretório do projeto, existe um arquivo chamado: "enquete.sql", este arquivo contém a criação do banco e tabelas para o banco de dados do projeto</p>
	<hr>
	<p><b>GET</b></p>
	<p>Para retornar uma enquete, deve acessar o arquivo: "poll", passando por parâmetro o ID da enquete, exemplo: http://url/poll/10 </p>
	<hr>
	<p><b>POST</b></p>
	<p>Para inserir uma enquete, deve acessar o arquivo: "poll", exemplo: http://url/poll </p>
	<hr>
	<p><b>POST VOTE</b></p>
	<p>Para votar em uma opção, deve acessar o arquivo: "poll", passando por parâmetro, o ID da opção e "vote",
	 exemplo: http://url/poll/10/vote , onde "10" é o ID da opção</p>
	<hr>
	<p><b>GET STATS</b></p>
	<p>Para retornar as estatísticas de uma enquete, deve acessar o arquivo: "poll", passando por parâmetro, o ID da enquete e "stats",
	 exemplo: http://url/poll/10/stats , onde "10" é o ID da enquete</p>
	<hr>
	<hr>
	<p><b>Adicionais</b></p>
	<p>Além de inserir a enquete e opções pela url, fiz um painel administrativo para criar, visualizar, editar e remover enquetes e opções, ao entrar no projeto, vai entrar direto no painel</p>
	<hr>
	<p><b>ENQUETES</b></p>
	<hr>
	<p><b>Como Adicionar uma enquete no painel administrativo</b></p>
	<p>Clique no menu lateral esquerdo em Enquetes, após isso, clique em adicionar, digite a descrição da Enquete e clique em salvar</p>
	<hr>
	<p><b>Como Editar uma enquete no painel administrativo</b></p>
	<hr>
	<p>Clique no menu lateral esquerdo em Enquetes, após isso, clique no ícone de lápis em uma das enquetes na tabela, digite a descrição da Enquete e clique em salvar</p>
	<hr>
	<p><b>Como Remover uma enquete no painel administrativo</b></p>
	<hr>
	<p>Clique no menu lateral esquerdo em Enquetes, após isso, clique no ícone de lixeira em uma das enquetes na tabela e clique em sim</p>
	<hr>
	<p><b>OPÇÕES</b></p>
	<hr>
	<p><b>Como Adicionar uma opção no painel administrativo</b></p>
	<p>Clique no menu lateral esquerdo em Enquetes, após isso, clique em Opções em uma das enquetes na tabela, agora irá listar todas as opções desta enquete, para adicionar clique em Adicionar no botão superior e insira uma descrição, após isso, clique em salvar</p>
	<hr>
	<p><b>Como Editar uma opção no painel administrativo</b></p>
	<hr>
	<p>Clique no menu lateral esquerdo em Enquetes, após isso, clique em Opções em uma das enquetes na tabela, clique no ícone de lápis em uma das opções na tabela, digite a descrição da Opção e clique em salvar</p>
	<hr>
	<p><b>Como Remover uma opção no painel administrativo</b></p>
	<hr>
	<p>Clique no menu lateral esquerdo em Enquetes, após isso, clique em Opções em uma das enquetes na tabela, clique no ícone de lixeira em uma das opções na tabela e clique em sim</p>
	<hr>
	<p><b>Como Votar uma opção no painel administrativo</b></p>
	<hr>
	<p>Clique no menu lateral esquerdo em Enquetes, após isso, clique em Opções em uma das enquetes na tabela, clique em votar em uma das opções na tabela</p>
</body>
</html>