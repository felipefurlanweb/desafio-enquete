<?php
    include_once 'Roteador.php';
    
    $roteador = new Roteador();
    $pagina_abrir = $roteador->getControlador();
    $categoria = $roteador->parametro(1);
    $subitem = $roteador->parametro(2);
    $uri = $roteador->getRequestURI();

    $pagina_abrir = $uri[1];
    $pagina_abrir = trim($pagina_abrir);

    if($pagina_abrir == "") {
        include_once "home.php";
        exit(0);
    } else {  
        if (file_exists($pagina_abrir.".php")) {
            include_once $pagina_abrir.".php";
            exit(0);
        } else {
            include_once "home.php";
            exit(0);
        }
    }
?>