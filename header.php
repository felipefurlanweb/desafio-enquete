<?php 
if (!isset($_SESSION["poll"])) {
    session_start();
}
    $path = 'http://'.$_SERVER['HTTP_HOST'].'/enquete/';

    include_once 'banco.php';
    include_once 'engine/funcoes.php'; 
?>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Projeto Teste Enquete - LUXFACTA</title>

    <link href="<?php echo $path; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $path; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $path; ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo $path; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo $path; ?>css/custom.css" rel="stylesheet">
    <link href="<?php echo $path; ?>css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="<?php echo $path; ?>css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="<?php echo $path; ?>js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $path; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $path; ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $path; ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $path; ?>js/inspinia.js"></script>
    <script src="<?php echo $path; ?>js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo $path; ?>js/plugins/sweetalert/sweetalert.min.js"></script>

</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <h3 class="block m-t-xs"> 
                            <strong class="font-bold">
                                Usuário
                            </strong>
                        </h3>
                    </div>
                </li>
                <li>
                    <a href="<?php echo $path; ?>"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
                </li>
                <li>
                    <a href="<?php echo $path; ?>polls"><i class="fa fa-gears"></i> <span class="nav-label">Enquetes</span></a>
                </li>
            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
            </nav>
        </div>