<?php include_once 'header.php'; ?>
    
    <?php 
        $param = $uri[2];
        $id = $param;
        $query = "SELECT * FROM polls WHERE poll_id = $id";
        $query = mysql_query($query);
        $numRows = mysql_num_rows($query);
        if ($numRows == 0) {
            include_once '404.php';
            die();
        }       
        $res = mysql_fetch_assoc($query);
        $titulo = $res["poll_description"];
    ?>

    <!-- CONTEUDO -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Editar Enquete</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form action="<?php echo $path ?>engine/formularios.php" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="pollEdit">
                                <input type="hidden" name="idModel" value="<?php echo $id; ?>">
                                <div class="col-xs-12 col-md-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 form-group">
                                            <label>Descrição</label>
                                            <input type="text" class="form-control" name="titulo" value="<?php echo $titulo; ?>">
                                        </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12 marginTop text-right">
                                            <hr>
                                            <a href="<?php echo $path; ?>polls" class="btn btn-danger">Voltar</a>
                                            <input type="submit" class="btn btn-primary" value="Salvar">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIM CONTEUDO -->

<?php include_once 'footer.php'; ?>
